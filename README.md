## How this branch is different
 - Option to show the citizens' names in deeds
 - The background of the map is now blue
 - Compress images for a vast decrease in size at no loss of quality, with a bit of added computation time
 - Option to generate a full sized PNG that users with JavaScript disabled can view
 - Configurable tower and portal IDs

# WurmMapGen
Interactive tile map generator for Wurm Unlimited servers.
![Screenshot](screenshot.png)

## Features
- Flexible instant search for any marker on the map
- Realtime updated player location markers (optional)
- Extensive [configuration options](https://github.com/woubuc/WurmMapGen/wiki/Configuration)

## How to use
Read the [documentation](https://github.com/woubuc/WurmMapGen/wiki)

## Master branch
When using this application in production, never directly use the code
from the master branch, as it may contain unfixed bugs or unfinished
features. Always use the downloads on the releases page.

## Contributing
If you want to contribute by fixing bugs or adding features, I welcome
pull requests.
